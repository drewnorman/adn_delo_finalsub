<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/7/2017
 * Time: 10:28 AM
 */

include_once "../php/activity_one_view.class.php";
include_once "../php/activity_two_view.class.php";
include_once "../php/activity_three_view.class.php";

// Filter the POST data
$id = filter_input(INPUT_POST, "activityId", FILTER_SANITIZE_STRING);

// Create the variable to hold our content
$content = "";

// Determine what to display
if ($id == 1) {
    // Display activity one
    $content = new Activity_One_View();
}
else if ($id == 2) {
    // Display activity two
    $content = new Activity_Two_View();
}
else if ($id == 3) {
    // Display activity three
    $content = new Activity_Three_View();
}

// Create the back button
$backButton = "<button id='back_button' onclick='returnToMenu();'>Return to Menu</button>";

// Echo the content and the back button
echo $content . $backButton;