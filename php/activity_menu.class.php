<?php

/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/7/2017
 * Time: 9:08 AM
 */
class Activity_Menu
{
    // Construct the activity menu
    public function __construct() {
        return $this->__toString();
    }

    // Return the menu as a string
    public function __toString()
    {
        // Create the activity menu container
        $container = "<div id='activity_menu'>";

        // Add the header
        $container .= "<h2>Please Select an Activity to View</h2>";

        // Add the first activity select button
        $container .= "<button data-activity-id=1 onclick='viewActivity(this);'>Activity One</button>";

        // Add the second activity select button
        $container .= "<button data-activity-id=2 onclick='viewActivity(this);'>Activity Two</button>";

        // Add the third activity select button
        $container .= "<button data-activity-id=3 onclick='viewActivity(this);'>Activity Three</button>";

        // Close the container
        $container .= "</div>";

        // Return the container
        return $container;
    }

}