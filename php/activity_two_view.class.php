<?php

/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 1/8/2017
 * Time: 8:46 AM
 */
class Activity_Two_View
{
    // Construct the view
    public function __construct()
    {
        return $this->__toString();
    }

    // Build the HTML
    public function __toString()
    {
        // Create the course registration container
        $container = "<div id='activity_two_view'>
                       <h2>Course Registration Form</h2><br><br>
                       <p>Please provide your student information and the relevant course data. </p><br><br><br>
                       <label>First Name: </label>
                       <input id='first_name_input' type='text'><br><br>
                       <label>Last Name: </label>
                       <input id='last_name_input' type='text'><br><br>
                       <label>WKU ID: </label>
                       <input id='wku_id_input' type='text'><br><br>
                       <label>Course Number: </label>
                       <input id='course_number_input' type='text'><br><br>
                       <label>Course Semester: </label>
                       <select id='course_semester_input'>
                       <option value='Summer 2017'>Summer 2017</option>
                       <option value='Fall 2017'>Fall 2017</option>
                       <option value='Winter 2017'>Winter 2017</option>
                       <option value='Spring 2018'>Spring 2018</option>
                       </select><br><br>
                       <p id='invalid_form_message'></p><br>
                       <button id='submit_registration_button' onclick='registerForCourse();'>Register</button><br><br>
                     </div>";

        // Return the form
        return $container;
    }

    // Get the successful registration popup
    public static function getSuccessfulRegistrationPopup($firstName, $lastName, $wkuId, $courseNumber, $courseSemester) {
        // Create the clickout
        $clickout = "<div id='successful_course_registration_clickout' class='clickout' onclick='closeSuccessfulRegistrationPopup();'></div>";

        // Create the container
        $container = "<div id='successful_course_registration_view'>
                <h2>Successful Registration!</h2><br>
                <hr><br>
                <p>First Name: $firstName</p><br>
                <p>Last Name: $lastName</p><br>
                <p>WKU ID: $wkuId</p><br>
                <p>Course Number: $courseNumber</p><br>
                <p>Course Semester: $courseSemester</p><br><br>
                </div>";

        // Return the clickout and container
        return $clickout . $container;
    }
}